
/* Entrada y salida de datos */ 

#include <iostream>
#include <stdio.h>
#include "IO_data.h"

using namespace std;

int leer_binario(char const *archivo_entrada, COMPLEX *data) {
	FILE* pArchivo;

	// Abrimos el archivo para leer en binario
	pArchivo = fopen(archivo_entrada, "rb");

	// Devolver error si el archivo no existe
	if (pArchivo == NULL) {
		cout << "Binario no encontrado: " << archivo_entrada << endl;
		return (EXIT_FAILURE);
	}

	cout << "Leyendo binario " << archivo_entrada << endl;

	// Volcamos el archivo directo a memoria
	fread(data, sizeof(COMPLEX), M*N, pArchivo);

	return (EXIT_SUCCESS);
}

int generar_archivo_binario(COMPLEX *data, char const *archivo_salida) {
	FILE* pArchivo;

	cout << "Generando binario: " << archivo_salida << endl;

	// Abrimos el archivo para escribir en binario
	pArchivo = fopen(archivo_salida, "wb");

	// Guardamos la matriz de M*N*sizeof(COMPLEX) bytes
	fwrite(data, sizeof(COMPLEX), M*N, pArchivo);
	fclose(pArchivo);

	return (EXIT_SUCCESS);
}

int leer_matriz_compleja(COMPLEX *data, char const *datos_real, char const *datos_imag) {
	int i,j;
	
	/* variables auxiliares para leer de archivos y armar el complejo */
	COMPLEX aux_c;
	FLOAT aux_real;
	FLOAT aux_imag;

	FILE *f_in_real, *f_in_imag;

	f_in_real = fopen(datos_real, "r");
	f_in_imag = fopen(datos_imag, "r");

	if (!f_in_real || !f_in_imag) {cout << "no puede abrir archivo  " << endl; exit (EXIT_FAILURE);};

	cout << "Leyendo archivo " << datos_real << endl;
	cout << "Leyendo archivo " << datos_imag << endl;

	for(i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			fscanf(f_in_real, "%f ", &aux_real);
			fscanf(f_in_imag, "%f ", &aux_imag);

			aux_real = (isnan(aux_real))? (FLOAT)0.0 : aux_real;
			aux_imag = (isnan(aux_imag))? (FLOAT)0.0 : aux_imag;

			aux_c = aux_real + aux_imag * I;
			data[i*N + j] = aux_c;
		}
	}

	fclose(f_in_real);
	fclose(f_in_imag);
	cout << "Archivos leidos." << endl;

	return(EXIT_SUCCESS);
}

int imprimir_matriz_compleja_a_archivo(COMPLEX *data, int filas, int cols, char const *nombre) {
	FILE *f_out;

	f_out = fopen(nombre, "w+");

	if (!f_out) {cout << "no puede abrir archivo  " << endl; exit (EXIT_FAILURE);};

	int i,j;
	for(i = 0; i < filas; i++){
		for(j = 0; j < cols; j++)
			if (isnan(creal(data[i*cols + j])))
				fprintf(f_out, "%.6f ", (float)0.0);
			else
				fprintf(f_out, "%.6f ", creal(data[i*cols + j]));
		
		fprintf(f_out, "\n");
	}

	fclose(f_out);
	return(EXIT_SUCCESS);
}

int imprimir_matriz_compleja_a_archivo_log(COMPLEX *data, int filas, int cols, char const *nombre) {
	FILE *f_out;

	f_out = fopen(nombre, "w+");

	if (!f_out) {cout << "no puede abrir archivo  " << endl; exit (EXIT_FAILURE);};

	int i,j;
	for(i = 0; i < filas; i++){
		for(j = 0; j < cols; j++)
			if (isnan(log(creal(data[i*cols + j]))))
				fprintf(f_out, "%.6f ", (float)0.0);
			else
				fprintf(f_out, "%.6f ", logf(creal(data[i*cols + j])));
			
		fprintf(f_out, "\n");
	}

	fclose(f_out);
	return(EXIT_SUCCESS);
}

int imprimir_matriz_float_a_archivo(FLOAT *data, int filas, int cols, char const *nombre) {
	FILE *f_out;

	f_out = fopen(nombre, "w+");

	if (!f_out) {cout << "no puede abrir archivo  " << endl; exit (EXIT_FAILURE);};

	int i,j;
	for(i = 0; i < filas; i++){
		for(j = 0; j < cols; j++)
			fprintf(f_out, "%.6f ", data[i*cols + j]);
		fprintf(f_out, "\n");
	}

	fclose(f_out);
	return(EXIT_SUCCESS);
}

int imprimir_vector_float(FLOAT *vector, int dim, char *nombre) {
	FILE *f_out;

	f_out = fopen(nombre, "w+");

	if (!f_out) {cout << "no puede abrir archivo  " << endl; exit (EXIT_FAILURE);};

	for(int i = 0; i < dim; i++) {
		fprintf(f_out, "%.6f \n", (vector[i]));
	}

	fclose(f_out);
	return(EXIT_SUCCESS);
}


int imprimir_vector_complex(COMPLEX *vector, int dim, char *nombre) {

	FILE *f_out;

	f_out = fopen(nombre, "w+");

	if (!f_out) {cout << "no puede abrir archivo  " << endl; exit (EXIT_FAILURE);};

	int i;
	for(i = 0; i < dim; i++){
		fprintf(f_out, "%.6f \n", (creal(vector[i])));
	
	}

	fclose(f_out);

	return(EXIT_SUCCESS);

}


int imprimir_fila_de_matriz_complex(COMPLEX *data, int fila, int  Nfilas, int Ncols, int desde, int hasta) {


	FILE *archivo;

	archivo = fopen("fila.out", "w");

	if (!archivo) {cout << "no puede abrir archivo  " << endl; exit (EXIT_FAILURE);};

	int kk;
	for(kk=desde; kk < hasta; kk++)
		fprintf(archivo, "%f  \n" , creal(data[fila * Ncols + kk]));


	fclose(archivo);

	return(EXIT_SUCCESS);
}


