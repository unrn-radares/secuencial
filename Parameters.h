/* Application constant definitions */

#pragma once

#include <stdio.h>
#include <complex.h>

/* cambiar a double o float as you want */ 
/* CAMBIAR DE FORM ACONSITENTE, NO ME DEJA NVCC PONER DIRECTAMENTE FOAT complex COMPLEX ...*/
typedef float FLOAT;
typedef float complex COMPLEX;



/* DIMENSIONES DE LOS DATOS */
#ifndef M
#define M 2242  // m
#endif

#ifndef N
#define N 13716  // n  = pulsos
#endif

#ifndef K
#define K  2 // k
#endif


#ifndef CODIGO_OPTIMIZACION
#define CODIGO_OPTIMIZACION 2 // 0: sin optimizar, 1: usando BLAS, 2: solo calculo de la diagonal 
#endif 


/* ARCHIVOS DE ENTRADA*/
#ifndef DATOS_ENTRADA_DATA_1_REAL
#define DATOS_ENTRADA_DATA_1_REAL "inputs/data_1_real.dat"
#endif


#ifndef DATOS_ENTRADA_DATA_1_IMAG
#define DATOS_ENTRADA_DATA_1_IMAG "inputs/data_1_imag.dat"
#endif


#ifndef DATOS_ENTRADA_DATA_2_REAL
#define DATOS_ENTRADA_DATA_2_REAL "inputs/data_2_real.dat"
#endif


#ifndef DATOS_ENTRADA_DATA_2_IMAG
#define DATOS_ENTRADA_DATA_2_IMAG "inputs/data_2_imag.dat"
#endif

/* BINARIOS A GENERAR */
#ifndef BINARIO_DATA_1
#define BINARIO_DATA_1 "inputs/data_1.bin"
#endif

#ifndef BINARIO_DATA_2
#define BINARIO_DATA_2 "inputs/data_2.bin"
#endif

/* PARA ARRANCAR RAPIDO, NO PROCESO HEADER.MAT SINO QUE SACO YA LAS VARIABLES */

#ifndef PRIMER_PULSO_BARRIDO
#define PRIMER_PULSO_BARRIDO 36   // en C comienzan los arreglos en 0 -> 36 y no 37 como en matlab
#endif

#ifndef ULTIMO_PULSO_BARRIDO
#define ULTIMO_PULSO_BARRIDO 13716
#endif

#ifndef PULSOS_GRUPO
#define PULSOS_GRUPO 36   // 35 pulsos al mismo lugar = azimuth = elevation
#endif

#ifndef BARRIDO
#define BARRIDO 2
#endif
