CC = nvcc 
CPPFLAGS = 
IDIR = /home/mdenham/CBLAS/include 

CFLAGS= -I$(IDIR)

BIN=main
SOURCES=$(shell echo *.cu)
OBJECTS=$(patsubst %.cu, %.o, $(SOURCES))
LDFLAGS = 
LIBS = -lblas 
# rules

all: $(BIN)

$(BIN): $(OBJECTS)
	$(CC) $(CFLAGS) $(LIBS) $(LDFLAGS) -o $@ $^ 

%.o: %.cu
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LIBS) -o $@ -c $<

-include .depend

.depend: $(SOURCES) *.h
	$(CC) -M $(CPPFLAGS) $(SOURCES) > .depend

.PHONY: all clean

clean: 
	rm -f .depend *.o $(BIN)
