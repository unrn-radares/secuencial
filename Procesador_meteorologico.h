#include <stdio.h>
#include "Parameters.h"

int procesar_datos(COMPLEX *data_V, COMPLEX *data_H);

int calcular_potencia_peque();

int calcular_potencia(COMPLEX *data, FLOAT *Po);
int calcular_potencia_usando_BLAS(COMPLEX *data, FLOAT *Po);
int calcular_potencia_optimizada1(COMPLEX *DATA, FLOAT *Po);

int calcular_frecuencia(COMPLEX *data, FLOAT *Freq);
int finalizar_calculo_frecuencia(COMPLEX *vector, int size, FLOAT *Freq);

int calcular_frecuencia_usando_BLAS(COMPLEX *data, FLOAT *Freq);
int calcular_frecuencia_optimizada1(COMPLEX *data, FLOAT *Freq);

int calcular_autocorrelacion(COMPLEX *data1, int desdeCol1, int hastaCol1, COMPLEX* data2, int desdeCol2, int hastaCol2, COMPLEX * R);
int calcular_corrimiento_fase(COMPLEX *R_vh, COMPLEX *prodPhi);
int calcular_coeficiente_correlacion(COMPLEX *R_vh, FLOAT *R_Poh, FLOAT *R_Pov, FLOAT *R_rho);

int guardar_datos_grupo_complejo(COMPLEX *matriz_complex, FLOAT *vector_float, int fila , int cantCols);
int guardar_datos_grupo_float(FLOAT *matriz_float, FLOAT *vector, int fila, int cantCols);
int guardar_datos_grupo_complejo_desde_vector_complejo(COMPLEX *matriz_complex, COMPLEX *vector_complex, int fila , int cantCols);

int armar_datos_grupo(COMPLEX *dv1, COMPLEX *d1h, COMPLEX *data_V, COMPLEX *data_H, int i);

int imprimir_fila_de_matriz_complex(COMPLEX *data, int fila, int  Nfilas, int Ncols, int desde, int hasta);
