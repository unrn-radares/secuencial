#include <cblas.h>
#include <iostream>
#include "Operaciones_matriz.h"
#include <stdlib.h>
#include "IO_data.h"




using namespace std;


/* 				BODIES        */ 
int matriz_compleja_traspuesta_conjugada(COMPLEX *data, int filas, int cols, COMPLEX *data_aux){

	int f,c;

	for(f = 0; f < filas; f++)
		for (c = 0; c < cols; c++)
			data_aux[ c * filas + f] = conj(data[ f * cols + c]);

	return(EXIT_SUCCESS);
}


int matriz_compleja_producto(COMPLEX *matriz1, int filas1, int cols1, COMPLEX *matriz2, int filas2, int cols2, COMPLEX *matriz_resultado) {

	COMPLEX aux;
	int f,c, k;
	
	if (cols1 != filas2) { cout << "Error en dimensiones de matrices en MM " << endl; exit(EXIT_FAILURE); }


	for (f = 0; f < filas1; f++) {
		for(c = 0 ; c < cols2; c++ ) {
			aux = 0.0 + 0.0 * I;
			for (k = 0; k < cols1 ; k++) {
				aux = aux + (matriz1[f * cols1 + k] * matriz2[ k * cols2 + c] );
			}
			matriz_resultado[f*cols2 + c] = aux;
		}	
	
	}

	return(EXIT_SUCCESS);
}


int matriz_compleja_diagonal(COMPLEX *matriz, int filas, int cols, FLOAT *diagonal){

	int f;

	if (filas != cols) { cout << "Error en traspuesta no es cuadrada la matriz " << endl; exit(EXIT_FAILURE); }
	
	for (f = 0; f < filas; f++) {
		diagonal[f] = creal(matriz[f * cols + f]);
	}


	return(EXIT_SUCCESS);
}



int matriz_compleja_producto_para_frecuencia(COMPLEX *matriz1, int filas_real_matriz1, int cols_real_matriz1, int desde_col, int hasta_col,
											 COMPLEX *matriz2, int filas_real_matriz2, int cols_real_matriz2, int desde_fila, int hasta_fila, 
											 COMPLEX *resultado_producto) {
	COMPLEX aux;
	int f,c, k;
	
	if (hasta_col-desde_col != hasta_fila-desde_fila) { cout << "Error en dimensiones de matrices en MM freq " << endl; exit(EXIT_FAILURE); }

	int cantidad_cols = hasta_col - desde_col;

	for (f = 0; f < filas_real_matriz1; f++) {
		for(c = 0 ; c < cols_real_matriz2; c++ ) {
			aux = 0.0 + 0.0 * I;
			for (k = 0; k < cantidad_cols ; k++) {
				aux = aux + (matriz1[f * cols_real_matriz1 + k] * matriz2[ (k+desde_fila) * cols_real_matriz2 + c] );
			}
			resultado_producto[f* cols_real_matriz2 + c] = aux;
		}	
	
	}	

	return(EXIT_SUCCESS);

}


int matriz_compleja_diagonal_complejo(COMPLEX *matriz, int filas , int cols , COMPLEX *resultado) {


	if (filas != cols) { cout << "Error en diagonal, la matriz no es cuadrada " << endl; exit(EXIT_FAILURE); }


	int j;
	for (j = 0; j < filas ; j ++) {

		resultado [j] = matriz [j * cols + j];
	}


	return(EXIT_SUCCESS);
}



int multiplicar_matrices_cblas_gemm(COMPLEX *matriz, int filas, int cols, COMPLEX *resultado) {

	//scalar factors
	COMPLEX alpha = (1.0 + 0.0*I);
	COMPLEX beta = (0.0 + 0.0*I);

	/* estas "leading dimension" son la distancia entre 2 elementos:
	 - rowMajorOrder: es la distancia entre 2 elementos de la misma columna (cantidad de columnas 
	 - colRajorOrder: es la distancia entre 2 elementos de la misma fila (cantidad de filas */

	cblas_cgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, filas, filas, cols, &alpha, matriz, cols, matriz, cols, &beta, resultado, filas);

	return(EXIT_SUCCESS);
}



int multiplicar_matrices_para_frecuencia_cblas_gemm(COMPLEX *matriz, int filas, int cols, COMPLEX *resultado) {


//scalar factors
	COMPLEX alpha = (1.0 + 0.0*I);
	COMPLEX beta = (0.0 + 0.0*I);

	/* estas "leading dimension" son la distancia entre 2 elementos:
	 - rowMajorOrder: es la distancia entre 2 elementos de la misma columna (cantidad de columnas 
	 - colRajorOrder: es la distancia entre 2 elementos de la misma fila (cantidad de filas */


	// lo llamo con A+1 para que comience con la columna 1 y no la 0
	cblas_cgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, filas, filas, cols-1, &alpha, matriz, cols, (matriz+1), cols, &beta, resultado, filas);

	return(EXIT_SUCCESS);	
	
}



/*   CBLAS   */
/*

cgemm (CBLAS_ORDER, TRANSA, TRANSB, M, N, K, &alpha, A, lda, B, ldeb, &beta, C, ldc)

C <- alpha * op(A) * op(B)  + beta * C

donde

CBLAS_ORDER = CblasRowMajor, CblasColMajor:  especifica como están almacendas las matrices: por filas (c, c++) o por columnas (fortran)

TRANSA, TRANSB = CblasNoTrans, CblasTrans, CblasConjTrans (no traspuesta, traspuesta, traspuesta conjugada para complejos) tambien 'N', 'T', 'C'

M = filas de A
N = Columnas de B
K = Columnas de A y filas de B

alpha = 1, beta = 0 en nuestro caso. 

lda, ldb, ldc = leading dimension of a, b and c: son los saltos entre un elemento y otro de la dimension "no contigua" = si almaceno por filas, 
				es la distancia entre un elemento y el mismo elemento de la próxima columna. Si almaceno por columnas, es la distancia entre un elemento
				y el mismo en la proxima fila -> CblasRowMajor lda = columnas de A, en tanto que si tengo CblasColMajor lda = filas de A. 



*/




















/*   PRUEBAS*/
int prueba_blas() {
double m[] = {
  3, 1, 3,
  1, 5, 9,
  2, 6, 5
};

double x[] = {
  -1, -1, 1
};

double y[] = {
  0, 0, 0
};


  int i, j;

  for (i=0; i<3; ++i) {
    for (j=0; j<3; ++j) printf("%5.1f", m[i*3+j]);
    putchar('\n');
  }

  cblas_dgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1.0, m, 3,
	      x, 1, 0.0, y, 1);

  for (i=0; i<3; ++i)  printf("%5.1f\n", y[i]);

  return 0;
}




int complex_transpose_blas() {
	COMPLEX *A,*C; //*B,

	int filasA = 22;
	int  colsA = 3;


	A = (COMPLEX*)malloc(sizeof(COMPLEX) * filasA * colsA);
//B = (COMPLEX*)malloc(sizeof(COMPLEX) * filasA * colsA);
	C = (COMPLEX*)malloc(sizeof(COMPLEX) * filasA * filasA);



  int i, j;
  COMPLEX aux;

  printf("Hola \n");
  for (i = 0; i < filasA; i++) {
    for (j = 0; j < colsA; j++) 
    {  
    	
		aux = ((float)(i+1) + (float)(i+1)*I);
    	A[i*colsA + j] = aux;
    //	B[i*colsA + j] = aux;
    } 
  }


  for (i = 0; i < filasA; i++) {
    for (j = 0; j < colsA; j++) 
    {  
    	printf("(%.1f %.1fi) ", creal(A[i*colsA + j]), cimag(A[i*colsA + j])) ;
    }
    printf("\n");
  }

printf("Chau \n");

 /*for (i = 0; i < filasB; i++) {
    for (j = 0; j < colsB; j++) 
    {  
    	aux = (i==j)? (1.0 + 0.0*i) : (0.0 + 0.0*i);
    	B[i*colsB + j] = aux;
    } 
    
 }
*/
	//scalar factors
	COMPLEX alpha = 1.0 + 0.0*i;
	COMPLEX beta = 0.0 + 0.0*i;

	/* estas "leading dimension" son la distancia entre 2 elementos:
	 - rowMajorOrder: es la distancia entre 2 elementos de la misma columna (cantidad de columnas 
	 - colRajorOrder: es la distancia entre 2 elementos de la misma fila (cantidad de filas */
  
//C <- alpha * op(A) * op(B)  + beta * C
	/* PERFECTO PARA a*a'*/
  //cblas_cgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, filasA, filasA, colsA, &alpha, A, colsA, A, colsA, &beta, C, filasA);

/* MODIFICA PARA FRECUENCIA:  a(:,1:end-1) * a(:,2:end)'    */
  
	// ANDA cblas_cgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, filasA, filasA, colsA, &alpha, A, colsA, A, colsA, &beta, C, filasA);
	cblas_cgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, filasA, filasA, colsA-1, &alpha, A, colsA, (A+1), colsA, &beta, C, filasA);


  for (i = 0; i < filasA; i++) {
    for (j = 0; j < filasA; j++) 
    {  
    	//printf("( %.1f %.1fi) ", creal(C[i*colsA + j]), cimag(C[i*colsA + j])) ;
    	printf("%.0f ", creal(C[i*filasA + j]) );
    }
    printf("\n");
  }



  	free(A);
  //	free(B);
  	free(C);

  return 0;
}

