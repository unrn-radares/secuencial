#include "Parameters.h"
//#include "/home/mdenham/CBLAS/include/cblas.h"




int matriz_compleja_traspuesta_conjugada(COMPLEX *data, int filas, int cols, COMPLEX *data_aux);

int matriz_compleja_producto(COMPLEX *matriz1, int filas1, int cols1, COMPLEX *matriz2, int filas2, int cols2, COMPLEX *matriz_resultado);

int matriz_compleja_diagonal(COMPLEX *matriz, int filas, int cols, FLOAT *diagonal);

int matriz_compleja_diagonal_complejo(COMPLEX *matriz, int filas , int cols , COMPLEX *resultado);


int matriz_compleja_producto_para_frecuencia(COMPLEX *matriz1, int filas_real_matriz1, int cols_real_matriz1, int desde_col, int hasta_col,
											 COMPLEX *matriz2, int filas_real_matriz2, int cols_real_matriz2, int desde_fila, int hasta_fila, 
											 COMPLEX *resultado_producto);


int multiplicar_matrices_cblas_gemm(COMPLEX *matriz, int filas, int cols, COMPLEX *resultado);

int multiplicar_matrices_para_frecuencia_cblas_gemm(COMPLEX *matriz, int filas, int cols, COMPLEX *resultado);


/*  BORRAR */
int prueba_blas();

int complex_transpose_blas();