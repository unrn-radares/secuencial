#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>

#include "Procesador_meteorologico.h"
#include "IO_data.h"
#include "Operaciones_matriz.h"
#include "cpu_timer.h"

using namespace std;

#define angle(x) (atan2(cimag(x),creal(x))) 


// Punteros a funciones para llamarlas segun codigo de optimizacion
typedef int (*p_funcion_potencia_type)(COMPLEX *, float *);
typedef int (*p_funcion_frecuencia_type)(COMPLEX *, float *);
typedef int (*p_funcion_autocorrelacion_type)(COMPLEX *, int, int, COMPLEX *, int, int, COMPLEX *);


int procesar_datos(COMPLEX *data_V, COMPLEX *data_H) {
	/*****************************************************/
	/**** Declaración de variables                    ****/
	/*****************************************************/

	// Punteros a funciones que dependen del codigo de optimizacion para los calculos
	p_funcion_potencia_type funcion_potencia;
	p_funcion_frecuencia_type funcion_frecuencia;
	p_funcion_autocorrelacion_type funcion_autocorrelacion;

	// Variables auxiliares para definir size de matrices float y complex
	int size_auxiliares = sizeof(FLOAT) * 360 * M;
	int size_auxiliares_complex = sizeof(COMPLEX) * 360 * M;

	// Grupos: serie de pulsos en la misma direccion (azimuth y elevation fijos), se procesan juntos
	COMPLEX *d1h, *d1v;
	d1h = (COMPLEX*) malloc((PULSOS_GRUPO+1) * M * sizeof(COMPLEX));
	d1v = (COMPLEX*) malloc((PULSOS_GRUPO+1) * M * sizeof(COMPLEX));

	// Frecuencia
	FLOAT *prodih, *prodiv;
	prodih = (FLOAT*) malloc(size_auxiliares);
	prodiv = (FLOAT*) malloc(size_auxiliares);
	
	// Potencia
	FLOAT *R_Poh, *R_Pov; // Arreglos usados en calculos posteriores
	R_Poh = (FLOAT*) malloc(M * sizeof(FLOAT));
	R_Pov = (FLOAT*) malloc(M * sizeof(FLOAT));
	
	COMPLEX *prodzh, *prodzv; // Ver por que pasan a complex
	prodzv = (COMPLEX*) malloc(size_auxiliares_complex);
	prodzh = (COMPLEX*) malloc(size_auxiliares_complex);

	// Autocorrelacion
	COMPLEX *R_a, *R_b, *R_vh;
	// Valores usados para ajustar la función en cada llamada
	int desdeCol1, hastaCol1, desdeCol2, hastaCol2;

	// Arreglos de M celdas porque la solucion secuencial procesa de a una fila (grupo)
	R_a = (COMPLEX*) malloc(sizeof(COMPLEX) * M);
	R_b = (COMPLEX*) malloc(sizeof(COMPLEX) * M);
	R_vh = (COMPLEX*) malloc(sizeof(COMPLEX) * M);
	
	COMPLEX *prodR_a, *prodR_b, *prodR_vh;
	prodR_a = (COMPLEX*) malloc(size_auxiliares_complex);
	prodR_b = (COMPLEX*) malloc(size_auxiliares_complex);
	prodR_vh = (COMPLEX*) malloc(size_auxiliares_complex);

	// Corrimiento de fase
	COMPLEX *prodR_phi, *R_phi;
	R_phi = (COMPLEX*) malloc(sizeof(COMPLEX) * M);
	prodR_phi = (COMPLEX*) malloc(size_auxiliares_complex);

	// Coeficiente de correlacion
	FLOAT *R_rho, *prodR_Rho;
	R_rho = (FLOAT*) malloc(size_auxiliares);
	prodR_Rho = (FLOAT*) malloc(size_auxiliares);

	// Arreglo auxiliar para algunos calculos
	FLOAT *vector_aux_float;
	vector_aux_float = (FLOAT*) malloc(M * sizeof(FLOAT));


	// Verifica que se hayan alocado correctamente los arreglos más grandes
	if (!d1h || !d1v || !prodih || !prodiv || !R_Poh || !R_Pov || !prodzh || !prodzv || !R_a || !R_b || !R_vh ||
		!prodR_a || !prodR_b || !prodR_vh || !R_phi || !prodR_phi || !R_rho || !prodR_Rho) {
		cout << "Alocacion de memoria falla" << endl;
		exit(EXIT_FAILURE);
	}

	// Timers para cronometrar productos y el proceso
	cpu_timer crono_potencia;
	cpu_timer crono_frecuencia;
	cpu_timer crono_autocorrelacion;
	cpu_timer crono_corrimiento_fase;
	cpu_timer crono_coef_correlacion;
	cpu_timer crono_total;

	double potencia_tiempo = 0.0;
	double frecuencia_tiempo = 0.0;
	double autocorrelacion_tiempo = 0.0;
	double corrimiento_fase_tiempo = 0.0;
	double coef_correlacion_tiempo = 0.0;


	/*****************************************************/
	/**** Asignar funciones a utilizar                ****/
	/*****************************************************/

	// Define las funciones a llamar según el código de optimización (punteros a funciones)
	// El objetivo de estos punteros es poder probar fácilmente distintas funciones.
	switch (CODIGO_OPTIMIZACION) {
		case (0): 
			funcion_potencia = calcular_potencia;
			funcion_frecuencia = calcular_frecuencia;	
			funcion_autocorrelacion = calcular_autocorrelacion;
			break;

		case (1): 
			funcion_potencia = calcular_potencia_usando_BLAS;
			funcion_frecuencia = calcular_frecuencia_usando_BLAS;
			funcion_autocorrelacion = calcular_autocorrelacion;
			break;

		case (2): 
			funcion_potencia = calcular_potencia_optimizada1; 
			funcion_frecuencia = calcular_frecuencia_optimizada1;
			funcion_autocorrelacion = calcular_autocorrelacion;
			break;

		default: 
			funcion_potencia = calcular_potencia_usando_BLAS; 
			funcion_frecuencia = calcular_frecuencia_usando_BLAS;
			funcion_autocorrelacion = calcular_autocorrelacion;
			break;
	}


	/*****************************************************/
	/**** Procesar productos secuencialmente          ****/
	/*****************************************************/

	crono_total.tic();
	for (int i = 0; i < 360; i++) {

		// Armar grupos d1h y d1v
		// ======================
		armar_datos_grupo(d1v, d1h, data_V, data_H, i);


		// Calcular potencia
		// =================

		// Poh (prodzh)
		crono_potencia.tic();	
			funcion_potencia(d1h, R_Poh);
		crono_potencia.tac();
		potencia_tiempo += crono_potencia.elapsed();

		guardar_datos_grupo_complejo(prodzh, R_Poh, i, M); /* prodzh(i,:,1)= Poh; */

		// Pov (prodzv)
		crono_potencia.tic();
			funcion_potencia(d1v, R_Pov); // Pov
		crono_potencia.tac();
		potencia_tiempo += crono_potencia.elapsed();

		guardar_datos_grupo_complejo(prodzv, R_Pov, i, M); /* prodzv(i,:,1)= Pov; */


		// Calcular frecuencia
		// ===================

		// d1h (prodih)
		crono_frecuencia.tic();
			funcion_frecuencia(d1h, vector_aux_float);
		crono_frecuencia.tac();
		frecuencia_tiempo += crono_frecuencia.elapsed();

		guardar_datos_grupo_float(prodih, vector_aux_float, i, M);

		// d1v (prodiv)
		crono_frecuencia.tic();
			funcion_frecuencia(d1v, vector_aux_float);
		crono_frecuencia.tac();
		frecuencia_tiempo += crono_frecuencia.elapsed();

		guardar_datos_grupo_float(prodiv, vector_aux_float, i, M);


		// Calcular autocorrelación
		// ========================

		// R_a
		/* R_a = (1/p)*dot(d1h(:,1:end-1),d1v(:,2:end),2); %R_vh[1] */
/*	desdeCol1 = 0;
		hastaCol1 = PULSOS_GRUPO-1;
		desdeCol2 = 1;
		hastaCol2 = PULSOS_GRUPO;	

		crono_autocorrelacion.tic();
			funcion_autocorrelacion(d1h, desdeCol1, hastaCol1, d1v, desdeCol2, hastaCol2, R_a);
		crono_autocorrelacion.tac();
		autocorrelacion_tiempo += crono_autocorrelacion.elapsed();

		guardar_datos_grupo_complejo_desde_vector_complejo(prodR_a, R_a, i, M);

		// R_b
		// R_b = (1/p)*dot(d1v(:,1:end-1),d1h(:,2:end),2); %R_hv[1] 
		desdeCol1 = 0;
		hastaCol1 = PULSOS_GRUPO-1;
		desdeCol2 = 1;
		hastaCol2 = PULSOS_GRUPO;

		crono_autocorrelacion.tic();
 			funcion_autocorrelacion(d1v, desdeCol1, hastaCol1, d1h, desdeCol2, hastaCol2, R_b);
		crono_autocorrelacion.tac();
		autocorrelacion_tiempo += crono_autocorrelacion.elapsed();

		guardar_datos_grupo_complejo_desde_vector_complejo(prodR_b, R_b, i, M);
*/
		// R_vh
		/* R_vh = (1/p)*dot(d1h,d1v,2); */
		desdeCol1 = 0;
		hastaCol1 = PULSOS_GRUPO;
		desdeCol2 = 0;
		hastaCol2 = PULSOS_GRUPO;

		crono_autocorrelacion.tic();
			funcion_autocorrelacion(d1h, desdeCol1, hastaCol1, d1v, desdeCol2, hastaCol2, R_vh);
		crono_autocorrelacion.tac();
		autocorrelacion_tiempo += crono_autocorrelacion.elapsed();
		
		guardar_datos_grupo_complejo_desde_vector_complejo(prodR_vh, R_vh, i, M);


		// Corrimiento de fase
		// ===================
		crono_corrimiento_fase.tic();
			calcular_corrimiento_fase(R_vh, R_phi);
		crono_corrimiento_fase.tac();
		corrimiento_fase_tiempo += crono_corrimiento_fase.elapsed();

		guardar_datos_grupo_complejo_desde_vector_complejo(prodR_phi, R_phi, i, M);


		// Coeficiente de correlación
		// ==========================
		crono_coef_correlacion.tic();
			calcular_coeficiente_correlacion(R_vh, R_Poh, R_Pov, R_rho);
		crono_coef_correlacion.tac();
		coef_correlacion_tiempo += crono_coef_correlacion.elapsed();

		guardar_datos_grupo_float(prodR_Rho, R_rho, i, M);

	}
	crono_total.tac();


	/*****************************************************/
	/**** Guardar datos en disco                      ****/
	/*****************************************************/

	// Potencia
	imprimir_matriz_compleja_a_archivo_log(prodzh, 360, M, "prodzh.out");
	imprimir_matriz_compleja_a_archivo_log(prodzv, 360, M, "prodzv.out");

	// Frecuencia
	imprimir_matriz_float_a_archivo(prodih, 360, M, "prodih.out");
	imprimir_matriz_float_a_archivo(prodiv, 360, M, "prodiv.out");

	// Autocorrelación
	imprimir_matriz_compleja_a_archivo_log(prodR_a, 360, M, "prodR_a.out");
	imprimir_matriz_compleja_a_archivo_log(prodR_b, 360, M, "prodR_b.out");
	imprimir_matriz_compleja_a_archivo_log(prodR_vh, 360, M, "prodR_vh.out");

	// Corrimiento de fase
	imprimir_matriz_compleja_a_archivo_log(prodR_phi, 360, M, "prodR_phi.out");

	// Coeficiente de correlación
	imprimir_matriz_float_a_archivo(prodR_Rho, 360, M, "prodR_rho.out");


	/*****************************************************/
	/**** Imprimir resultados                         ****/
	/*****************************************************/

	cout << "RMA: " << crono_total.elapsed() << " ms " <<  endl;
	cout << "Calculo de potencia: " << potencia_tiempo << " ms " <<  endl;
	cout << "Calculo de frecuencia: " << frecuencia_tiempo << " ms " <<  endl;
	cout << "Calculo de autocorrelacion: " << autocorrelacion_tiempo << " ms " <<  endl;
	cout << "Calculo corrimiento de fase: " << corrimiento_fase_tiempo << " ms " <<  endl;
	cout << "Calculo coef de correlacion: " << coef_correlacion_tiempo << " ms " <<  endl;


	/*****************************************************/
	/**** Liberar memoria                             ****/
	/*****************************************************/

	free(vector_aux_float);

	// Grupos
	free(d1h);
	free(d1v);
	
	// Frecuencia
	free(prodih);
	free(prodiv);

	// Potencia
	free(R_Poh);
	free(R_Pov);
	free(prodzh);
	free(prodzv);

	// Autocorrelacion
	free(R_a);
	free(R_b);
	free(R_vh);
	free(prodR_a);
	free(prodR_b);
	free(prodR_vh);

	// Corrimiento de fase
	free(R_phi);
	free(prodR_phi);
	
	// Coeficiente de correlacion
	free(R_rho);
	free(prodR_Rho);


	return (EXIT_SUCCESS);
}

/*
	Poh=diag(d1h*d1h');
	  (diag de matlab se queda solo con la parte real de la diagonal)
*/
int calcular_potencia(COMPLEX *data, FLOAT *Po) {
	// Variables auxiliares
	COMPLEX *resultado_traspuesta = (COMPLEX *) malloc(M * (PULSOS_GRUPO) * sizeof(COMPLEX));
	COMPLEX *resultado_producto = (COMPLEX *) malloc(M * M * sizeof(COMPLEX));

	if (!resultado_traspuesta || !resultado_producto) { cout << "No aloca calcular_potencia" << endl; exit(-1); }


	if (matriz_compleja_traspuesta_conjugada(data, M, PULSOS_GRUPO, resultado_traspuesta) != EXIT_SUCCESS) { cout << "Error en traspuesta " << endl; exit(EXIT_FAILURE); };
	if (matriz_compleja_producto(data, M, PULSOS_GRUPO, resultado_traspuesta, PULSOS_GRUPO, M, resultado_producto) != EXIT_SUCCESS)  { cout << "Error en producto matrices " << endl; exit(EXIT_FAILURE); };
		
	if (matriz_compleja_diagonal(resultado_producto, M , M, Po) != EXIT_SUCCESS) {cout << "Error en diagonal matriz " << endl; exit(EXIT_FAILURE); } ;
	
	
	free(resultado_traspuesta);
	free(resultado_producto);
	return(0);
}


/*  Poh=diag(d1h*d1h'); */
int calcular_potencia_usando_BLAS(COMPLEX *data, FLOAT *Po) {

	COMPLEX *resultado_producto;
	resultado_producto = (COMPLEX *) malloc(M * M * sizeof(COMPLEX));

	
	if ( !resultado_producto) { cout << "No aloca calcular_potencia" << endl; exit(-1); }

	/* llamado a cblas_cgem */
	multiplicar_matrices_cblas_gemm(data, M, PULSOS_GRUPO, resultado_producto);
		
	if (matriz_compleja_diagonal(resultado_producto, M , M, Po) != EXIT_SUCCESS) {cout << "Error en diagonal matriz " << endl; exit(EXIT_FAILURE); } ;

	free(resultado_producto);
	return(0);
}



int calcular_potencia_optimizada1(COMPLEX *data, FLOAT *Po) {

	int f, k;
	COMPLEX total;

	/* recorro cada fila para armar Po */ 
	for (f = 0; f < M; f++){

		total = 0.0 + 0.0 * I;

		for (k = 0; k < PULSOS_GRUPO; k++) {
			total += data[f*PULSOS_GRUPO + k] * conj(data[f*PULSOS_GRUPO + k]);
		}
		Po[f] = creal(total);

	}
	return(EXIT_SUCCESS);

}

/*
	sd1h=diag(d1h(:,1:end-1)*d1h(:,2:end)');
	foh=-angle(sd1h)/2/pi/T;
*/
int calcular_frecuencia(COMPLEX *data, FLOAT *Freq){

	COMPLEX *resultado_producto, *resultado_traspuesta, *resultado_diagonal_complejo;
	//sd1 = (COMPLEX*) malloc(M * sizeof(COMPLEX));
	resultado_traspuesta = (COMPLEX*) malloc(M * (PULSOS_GRUPO) * sizeof(COMPLEX));
	resultado_producto = (COMPLEX*) malloc(M * M *sizeof(COMPLEX));
	resultado_diagonal_complejo = (COMPLEX*) malloc(M * sizeof(COMPLEX));

	if (matriz_compleja_traspuesta_conjugada(data, M, PULSOS_GRUPO, resultado_traspuesta) != EXIT_SUCCESS) { cout << "Error en traspuesta " << endl; exit(EXIT_FAILURE); };

	// llamo a una funcion especial, para evitar más arreglos, le digo que columnas calcular. desde hasta
	// sd1h=diag(d1h(:,1:end-1)*d1h(:,2:end)');
	if (matriz_compleja_producto_para_frecuencia(data, M, (PULSOS_GRUPO), 0, (PULSOS_GRUPO)-1,
											 	 resultado_traspuesta, (PULSOS_GRUPO), M, 1, PULSOS_GRUPO, 
												 resultado_producto) != EXIT_SUCCESS) {cout << "Error en MM frequencia " << endl; exit(EXIT_FAILURE); }; 

	//if (matriz_compleja_producto_para_frecuencia(data, M, 0, (PULSOS_GRUPO+1)-1, resultado_traspuesta, 1,PULSOS_GRUPO+1, M, resultado_producto) != EXIT_SUCCESS)  { cout << "Error en producto matrices " << endl; exit(EXIT_FAILURE); };
		
	if (matriz_compleja_diagonal_complejo(resultado_producto, M , M, resultado_diagonal_complejo) != EXIT_SUCCESS) {cout << "Error en diagonal matriz " << endl; exit(EXIT_FAILURE); } ;

	//foh=-angle(sd1h)/2/pi/T;
	if (finalizar_calculo_frecuencia(resultado_diagonal_complejo, M, Freq) != EXIT_SUCCESS ) {cout << "Error en calcular frecuencia " << endl; exit(EXIT_FAILURE); };

	free(resultado_producto);
	free(resultado_traspuesta);
	free(resultado_diagonal_complejo);

	return 0;
}



int calcular_frecuencia_usando_BLAS(COMPLEX *data, FLOAT *Freq){

	COMPLEX *resultado_producto, *resultado_diagonal_complejo;
	resultado_producto = (COMPLEX *) malloc(M * M * sizeof(COMPLEX));
	resultado_diagonal_complejo = (COMPLEX *) malloc(M * sizeof(COMPLEX));

	
	if ( !resultado_producto || !resultado_diagonal_complejo) { cout << "No aloca calcular_potencia " << endl; exit(-1); }

	/* llamado a cblas_cgem */
	multiplicar_matrices_para_frecuencia_cblas_gemm(data, M, PULSOS_GRUPO, resultado_producto);
		
	
	if (matriz_compleja_diagonal_complejo(resultado_producto, M , M, resultado_diagonal_complejo) != EXIT_SUCCESS) {cout << "Error en diagonal matriz " << endl; exit(EXIT_FAILURE); } ;

	//foh=-angle(sd1h)/2/pi/T;
	if (finalizar_calculo_frecuencia(resultado_diagonal_complejo, M, Freq) != EXIT_SUCCESS ) {cout << "Error en calcular frecuencia " << endl; exit(EXIT_FAILURE); };

	free(resultado_producto);
	free(resultado_diagonal_complejo);

	return(EXIT_SUCCESS);

}



int calcular_frecuencia_optimizada1(COMPLEX *data, FLOAT *Freq) {

	int f, k;
	COMPLEX total;

	/* recorro cada fila para armar Po */ 
	for (f = 0; f < M; f++){

		total = 0.0 + 0.0 * I;

		for (k = 0; k < PULSOS_GRUPO-1; k++) {
			total += data[f*PULSOS_GRUPO + k] * conj(data[f*PULSOS_GRUPO + (k+1)]);
		}
		Freq[f] = -1.0 * ((angle(total) / 2.0f) /M_PI );
		//foh=-angle(sd1h)/2/pi/T;
 	

	}
	return(EXIT_SUCCESS);

}


//foh=-angle(sd1h)/2/pi/T;
int finalizar_calculo_frecuencia(COMPLEX *vector, int size, FLOAT *Freq){

	int j ;

	for (j = 0; j < size; j++) {
		Freq[j] = -1.0 * ((angle(vector[j]) / 2.0f) / M_PI  );  // T con T=1 segun matlab, no lo pongo
	}


	return (EXIT_SUCCESS);

}

/* dot usa el conjugado de la segunda matriz */
int calcular_autocorrelacion(COMPLEX *data1, int desdeCol1, int hastaCol1, COMPLEX* data2, int desdeCol2, int hastaCol2, COMPLEX * R){

	int f, k;
	COMPLEX total;

	int diferencia_columnas = hastaCol1 - desdeCol1;
	/* recorro cada fila para armar R */ 
	for (f = 0; f < M; f++){

		total = 0.0 + 0.0 * I;

		for (k = 0; k < diferencia_columnas; k++) {
			total += data1[f*PULSOS_GRUPO + (k+desdeCol1)] * conj(data2[f*PULSOS_GRUPO + (k+desdeCol2)]);
		}
		R[f] = ((FLOAT)creal(total) / (FLOAT)PULSOS_GRUPO)  + ((FLOAT) cimag(total) / (FLOAT) PULSOS_GRUPO) * I;
	//	R[f] = total;
	
	}
	return(EXIT_SUCCESS);
}

/*
	Phi = angle(R_vh);
	prodPhi(i,:) = Phi;
*/
int calcular_corrimiento_fase(COMPLEX *R_vh, COMPLEX *R_phi) {
	for (int i=0; i < M; i++) {
		R_phi[i] = angle(R_vh[i]);
	}
	return 0;
}

/*
	rho_0=R_vh./sqrt(Pov.*Poh);
	prodRho(i,:)=abs(rho_0);
*/
int calcular_coeficiente_correlacion(COMPLEX *R_vh, FLOAT *R_Poh, FLOAT *R_Pov, FLOAT *R_rho) {
	for (int i=0; i < M; i++) {
		R_rho[i] = cabsf(R_vh[i] / sqrt(R_Pov[i] * R_Poh[i]));
	}
	return 0;
}

int guardar_datos_grupo_complejo(COMPLEX *matriz_complex, FLOAT *vector_float, int fila , int cantCols) {

	int j;

	for(j = 0; j < cantCols; j++) {
		matriz_complex[fila * cantCols + j] = (vector_float[j] + 0.0*I);
	}


	return 0;
}

int guardar_datos_grupo_float(FLOAT *matriz_float, FLOAT *vector, int fila, int cantCols) {

	int j;

	for(j = 0; j < cantCols; j++)
		matriz_float[fila * cantCols + j] = (vector[j]);


	return 0;
}


int guardar_datos_grupo_complejo_desde_vector_complejo(COMPLEX *matriz_complex, COMPLEX *vector_complex, int fila , int cantCols) {
	
	int j;

	for(j = 0; j < cantCols; j++) {
		matriz_complex[fila * cantCols + j] = vector_complex[j];
		//matriz_complex[fila * cantCols + j] = (vector_float[j] + 0.0*I);
	}


	return 0;


}


int armar_datos_grupo(COMPLEX *d1v, COMPLEX *d1h, COMPLEX *data_V, COMPLEX *data_H, int grupo) {

	int f, c;

//	cout << "M: " << M <<  " N: " << N <<  " PRIMER PULSO BARRIDO: " << PRIMER_PULSO_BARRIDO << " Pulsos grupo: "<<  PULSOS_GRUPO << endl;
 //	cout << "Primer col: " << PRIMER_PULSO_BARRIDO + (grupo * (PULSOS_GRUPO)) << " hasta: " <<  PRIMER_PULSO_BARRIDO + (grupo * (PULSOS_GRUPO)) + (PULSOS_GRUPO)  <<endl;
		
	for (f = 0; f < M; f++) {
		
		// agrego +1 en columna, a ver si los puedo sacar despues
		for (c=0; c < PULSOS_GRUPO; c++) {
			//d1h[f* PULSOS_GRUPO + c] = data_H[ (f*N) + c];		
			d1h[(f* (PULSOS_GRUPO)) + c] = data_H[ (f*N) + (PRIMER_PULSO_BARRIDO + (grupo * (PULSOS_GRUPO)) + c) ];
			d1v[(f* (PULSOS_GRUPO)) + c] = data_V[ (f*N) + (PRIMER_PULSO_BARRIDO + (grupo * (PULSOS_GRUPO)) + c) ];
		}

	}

	return (0);
}

/*   CBLAS   */

/*

cgemm (CBLAS_ORDER, TRANSA, TRANSB, M, N, K, &alpha, A, lda, B, ldeb, &beta, C, ldc)

C <- alpha * op(A) * op(B)  + beta * C

donde

CBLAS_ORDER = CblasRowMajor, CblasColMajor:  especifica como están almacendas las matrices: por filas (c, c++) o por columnas (fortran)

TRANSA, TRANSB = CblasNoTrans, CblasTrans, CblasConjTrans (no traspuesta, traspuesta, traspuesta conjugada para complejos) tambien 'N', 'T', 'C'

M = filas de A
N = Columnas de B
K = Columnas de A y filas de B

alpha = 1, beta = 0 en nuestro caso. 

lda, ldb, ldc = leading dimension of a, b and c: son los saltos entre un elemento y otro de la dimension "no contigua" = si almaceno por filas, 
				es la distancia entre un elemento y el mismo elemento de la próxima columna. Si almaceno por columnas, es la distancia entre un elemento
				y el mismo en la proxima fila -> CblasRowMajor lda = columnas de A, en tanto que si tengo CblasColMajor lda = filas de A. 

*/
